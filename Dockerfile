ARG ALPINE_VERSION
ARG PYTHON_VERSION
ARG TMUXP_VERSION

FROM alpine:${ALPINE_VERSION} AS c-builder

ARG TMUX_VERSION
ARG TMUX_URL=https://github.com/tmux/tmux/releases/download/${TMUX_VERSION}/tmux-${TMUX_VERSION}.tar.gz
RUN apk add --no-cache \
        bison \
        build-base \
        curl \
        libevent-dev \
        ncurses-dev \
        pkgconf \
        tar && \
    curl -fLo tmux.tar.gz ${TMUX_URL} && \
    tar -xzf tmux.tar.gz && \
    cd tmux-${TMUX_VERSION} && \
    ./configure && make && DESTDIR=/tmux make install

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION} AS python-builder

ARG TMUXP_VERSION
RUN apk add --no-cache \
        build-base && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools \
        wheel && \
    mkdir -p /wheelhouse && \
    python3 -m pip wheel -w /wheelhouse --no-cache-dir \
        tmuxp==${TMUXP_VERSION}

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

ARG TMUXP_VERSION
COPY --from=python-builder /wheelhouse /wheelhouse
COPY --from=c-builder /tmux/usr/local/bin/ /usr/local/bin/
RUN apk add --no-cache \
        libevent \
	ncurses && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools && \
    python3 -m pip install --no-index --find-links=/wheelhouse --no-cache-dir \
        tmuxp==${TMUXP_VERSION}

CMD ["tmuxp"]
